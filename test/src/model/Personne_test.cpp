#include <gtest/gtest.h>

#include "./model/Personne.h"

namespace model
{
	TEST(Personne, Name)
	{
		Personne personne;
		EXPECT_EQ("Unknown", personne.getName());
	}

	TEST(Personne, PhoneNumber)
	{
		Personne personne;

		EXPECT_EQ("06XXXXXXXX", personne.getPhoneNumber());
	}

	TEST(Personne, Expenses)
	{
		Personne personne;

		EXPECT_EQ(0, personne.getExpenses());
	}
}

