#include <gtest/gtest.h>

#include "./model/Group.h"
#include "model/Personne.h"

namespace model
{
	TEST(Group, Name)
	{
		Group group;
		EXPECT_EQ("Undefined", group.getGroupName());
		group.setGroupName("test");
		EXPECT_EQ("test", group.getGroupName());
	}

	TEST(Group, Personne)
	{
		Group group;
		Personne personne("test", "1234", 21);

		group.addPersonneInGroup(personne);

		EXPECT_EQ(21, group.getTabPersonne().at(group.getPersonneIndexInGroup("test")).getExpenses());
	}

	TEST(Group, Expenses)
	{
		Group group("test");
		group.setTotalExpenses(51.2);

		EXPECT_EQ(51.2, group.getTotalExpenses());
	}
}


