#include <gtest/gtest.h>

#include "./controller/CSVParser.h"

namespace controller
{
	std::string fileName = "./ressources/test.txt";

	TEST(CSVParser, getLines)
	{
		CSVParser parser(fileName);

		EXPECT_EQ(20, parser.getNumberOfRows());
	}

	TEST(CSVParser, numberOfColumns)
	{
		CSVParser parser(fileName);

		EXPECT_EQ(4, parser.getNumberOfColumns());
	}

	TEST(CSVParser, getLine3)
	{
		CSVParser parser(fileName);
		std::vector<std::string> test = {"Benjamin", "9012", "114", "Jasse"};

		ASSERT_EQ(test, parser.getLine(3));
	}

	TEST(CSVParser, getHeader)
	{
		CSVParser parser(fileName);
		std::vector<std::string> test = {"Name", "Phone Number", "Expenses", "Group Name"};

		ASSERT_EQ(test, parser.getHeader());
	}
}

