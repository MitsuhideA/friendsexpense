#include <gtest/gtest.h>

#include "controller/ExpenseCalculator.h"
#include "model/DataExpenses.h"

namespace controller
{
	std::string fileNameExpenseTest = "./ressources/expenses_test.csv";

	TEST(ExpenseCalculator, calculateFromFile)
	{
		DataExpenses data;

		ExpenseCalculator calculator;
		calculator.calculateFromFile(fileNameExpenseTest, &data);

		EXPECT_EQ(548, data.groupes.at(0).getTotalExpenses());
		EXPECT_EQ(576, data.groupes.at(1).getTotalExpenses());
		EXPECT_EQ(472, data.groupes.at(2).getTotalExpenses());
		EXPECT_EQ(414, data.groupes.at(3).getTotalExpenses());
	}
}

