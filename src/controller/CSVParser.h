/*
 * CSVParser.h
 *
 *  Created on: May 31, 2016
 *      Author: zak
 */

#ifndef CSVPARSER_H_
#define CSVPARSER_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

namespace controller
{

class CSVParser
{
public:
	CSVParser();
	CSVParser(std::string fileName);
	int getNumberOfColumns();
	int getNumberOfRows();
	std::vector<std::string> getLine(int iRowNum);
	std::vector<std::string> getHeader();
	void setNewLine(std::string inputName, std::string inputPhoneNumber, double inputExpenses, std::string inputGroupName);
	~CSVParser();
private:
	std::string _fileName;
    const char* _separator = ",";
    std::vector<std::string> split(std::string str, std::string delimiter);
    void find_and_replace(std::string& source, std::string const& find, std::string const& replace);
};

}
#endif /* CSVPARSER_H_ */
