/*
 * CSVParser.cpp
 *
 *  Created on: May 31, 2016
 *      Author: zak
 */

#include "CSVParser.h"

namespace controller
{

CSVParser::CSVParser()
{

}

CSVParser::CSVParser(std::string fileName)
{
    _fileName = fileName;
}

CSVParser::~CSVParser()
{

}

std::vector<std::string> CSVParser::getHeader()
{
    std::ifstream f(_fileName.c_str(), std::ifstream::in);
    std::string line;

    std::getline(f, line);

    std::vector<std::string> separated = split(line, _separator);

    return separated;
}

std::vector<std::string> CSVParser::getLine(int iRowNum)
{
    std::ifstream f(_fileName.c_str(), std::ifstream::in);
    std::string line;
    int i;

    std::getline(f, line); // Ignore la première ligne qui est l'entête

    for(i = 0; i != iRowNum; i++)
    {
        std::getline(f, line); // Ignore la première ligne qui est l'entête
    }

    std::vector<std::string> separated = split(line, _separator);

    return separated;
}

int CSVParser::getNumberOfRows()
{
    std::ifstream f(_fileName.c_str(), std::ifstream::in);
    std::string line;
    int i;

    std::getline(f, line); // Ignore la première ligne qui est l'entête

    for (i = 0; std::getline(f, line); ++i);

    return i;
}

int CSVParser::getNumberOfColumns()
{
    std::ifstream f(_fileName.c_str(), std::ifstream::in);
    std::string line;
    int cpt = 0;

    std::getline(f, line);

    std::vector<std::string> separated = split(line, _separator);
    for(std::string t : separated)
    {
        cpt++;
    }

    return cpt;
}

void CSVParser::setNewLine(std::string inputName, std::string inputPhoneNumber, double inputExpenses, std::string inputGroupName)
{
	std::fstream f(_fileName.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);

	f << inputName << _separator << inputPhoneNumber << _separator << inputExpenses << _separator << inputGroupName << std::endl;

	f.close();
}

////////////////////////////////////////////
//              PRIVATE                   //
////////////////////////////////////////////


// You could also take an existing vector as a parameter.
std::vector<std::string> CSVParser::split(std::string str, std::string delimiter)
{
    if(delimiter == "    ")     // Si le délimiteur est 4 espaces on le remplace par \t pour faciliter le travail avec getline
    {
        find_and_replace(str, "    ", "\t");
        delimiter = "\t";
    }

    std::vector<std::string> internal;
    std::stringstream ss(str); // Turn the string into a stream.
    std::string tok;

    while(std::getline(ss, tok, delimiter.c_str()[0])) {
        internal.push_back(tok);
    }

    return internal;
}

void CSVParser::find_and_replace(std::string& source, std::string const& find, std::string const& replace)
{
    for(std::string::size_type i = 0; (i = source.find(find, i)) != std::string::npos;)
    {
        source.replace(i, find.length(), replace);
        i += replace.length();
    }
}

}
