/*
 * ExpenseCalculator.cpp
 *
 *  Created on: Jun 2, 2016
 *      Author: zak
 */

#include "ExpenseCalculator.h"

namespace controller
{

	ExpenseCalculator::ExpenseCalculator()
	{

	}

	ExpenseCalculator::~ExpenseCalculator()
	{

	}

	int ExpenseCalculator::calculateFromFile(std::string filename, DataExpenses* data)
	{
		CSVParser fileParser(filename);

		if(fileParser.getNumberOfColumns() != _nbOfColumnsInFile)
			return -1;

		for(int i = 1; i <= fileParser.getNumberOfRows(); i++)
		{
			// Line : Name(0) | Phone Number(1) | Expenses(2) | Group(3)
			std::vector<std::string> line = fileParser.getLine(i);

			if(line.size() != _nbOfColumnsInFile)
				continue;	// Ignore la ligne s'il y a plus de 4 colonnes

			double tempExpenses;

			try
			{
				tempExpenses = std::stod(line.at(2));
			} catch(...)
			{
				// Ignore la ligne si on ne peut pas convertir en double
				continue;
			}

			Personne tempPersonne(line.at(0), line.at(1), tempExpenses);
			int groupIndex = getGroupIndexFromData(line.at(3), data);
			int personneIndex = data->groupes.at(groupIndex).getPersonneIndexInGroup(tempPersonne.getName());
			if(personneIndex == -1)
			{
				data->groupes.at(groupIndex).addPersonneInGroup(tempPersonne);
			}
			else
			{
				int personneExpenses = data->groupes.at(groupIndex).getTabPersonne().at(personneIndex).getExpenses() + tempPersonne.getExpenses();
				data->groupes.at(groupIndex).getTabPersonne().at(personneIndex).setExpenses(personneExpenses);
			}
		}

		calculateGroupExpenses(data);
		return 0;
	}

	void ExpenseCalculator::calculateGroupExpenses(DataExpenses* data)
	{
		for(Group &g: data->groupes)
		{
			for(Personne &p: g.getTabPersonne())
			{
				g.setTotalExpenses(g.getTotalExpenses() + p.getExpenses());
			}

			double avgExpenses = g.getTotalExpenses() / g.getTabPersonne().size();

			for(Personne &p: g.getTabPersonne())
			{
				p.setDeltaExpenses(p.getExpenses() - avgExpenses);
			}
		}
	}

	int ExpenseCalculator::getGroupIndexFromData(std::string groupName, DataExpenses* data)
	{
		unsigned int i;

		for(i = 0; i < data->groupes.size(); i++)
		{
			if(data->groupes.at(i).getGroupName() == groupName)
			{
				return i;
			}
		}

		data->groupes.push_back(Group(groupName));

		return i;
	}

} /* namespace controller */
