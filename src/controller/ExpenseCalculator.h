/*
 * ExpenseCalculator.h
 *
 *  Created on: Jun 2, 2016
 *      Author: zak
 */

#ifndef CONTROLLER_EXPENSECALCULATOR_H_
#define CONTROLLER_EXPENSECALCULATOR_H_

#include <string>

#include "../model/DataExpenses.h"
#include "CSVParser.h"

using namespace model;

namespace controller
{
	class ExpenseCalculator
	{
		public:
			ExpenseCalculator();
			~ExpenseCalculator();
			int calculateFromFile(std::string filename, DataExpenses* data);

		private:
			int getGroupIndexFromData(std::string groupName, DataExpenses* data);
			void calculateGroupExpenses(DataExpenses* data);

			static const int _nbOfColumnsInFile = 4;
	};

} /* namespace controller */

#endif /* CONTROLLER_EXPENSECALCULATOR_H_ */
