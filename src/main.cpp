#include <iostream>
#include "./view/CmdParser.h"

using namespace std;
using namespace view;

int main(int argc, char *argv[])
{
	DataExpenses data;
	CmdParser cmdParser;

	cmdParser.initData(&data);
	cmdParser.checkParser(argc, argv);

	return 0;
}
