/*
 * Personne.h
 *
 *  Created on: 31 mai 2016
 *      Author: user
 */
#ifndef PERSONNE_H_
#define PERSONNE_H_

#include <string>
using namespace std;

namespace model
{

	class Personne
	{
		private:
			string name;
			string phoneNumber;
			double expenses;
			double deltaExpenses;

		public:
			Personne();
			Personne(string inputName, string inputPhoneNumber,double inputExpenses);
			//Personne(Personne &Personne);
			virtual ~Personne();
			string getName();
			void setName(string inputName);
			string getPhoneNumber();
			void setPhoneNumber(string inputPhoneNumber);
			double getExpenses();
			void setExpenses(double inputExpenses);
			void addExpensesToPersonne(double inputExpenses);
			double getDeltaExpenses();
			void setDeltaExpenses(double inputDeltaExpenses);
	};

}
#endif /* PERSONNE_H_ */
