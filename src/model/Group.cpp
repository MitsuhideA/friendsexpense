/*
 * group.cpp
 *
 *  Created on: 2 juin 2016
 *      Author: user
 */

#include "Group.h"

namespace model
{
	Group::Group()
	{
		name = "Undefined";
		totalExpenses = 0;
	}

	Group::Group(string inputName)
	{
		name = inputName;
		totalExpenses = 0;
	}

	Group::Group(string inputName, std::vector<Personne> inputTabPersonne,double inputTotalExpenses)
	{
		name = inputName;
		tabPersonne = inputTabPersonne;
		totalExpenses = inputTotalExpenses;
	}

	/*Group::Group(Group &Group)
	{
		name = Group.name;
		tabPersonne = Group.tabPersonne;
	}*/

	string Group::getGroupName()
	{
		return name;
	}

	void Group::setGroupName(string inputGroupName)
	{
		name = inputGroupName;
	}

	std::vector<Personne>& Group::getTabPersonne()
	{
		return tabPersonne;
	}

	void Group::addPersonneInGroup(Personne inputPersonne)
	{
		tabPersonne.push_back(inputPersonne);
	}

	double Group::getTotalExpenses()
	{
		return totalExpenses;
	}

	void Group::setTotalExpenses(double inputTotalExpenses)
	{
		totalExpenses = inputTotalExpenses;
	}

	int Group::getPersonneIndexInGroup(std::string name)
	{
		for(unsigned int i = 0; i < tabPersonne.size(); i++)
		{
			if(tabPersonne.at(i).getName() == name)
			{
				return i;
			}
		}
		return -1;
	}

	Group::~Group()
	{

	}
}

