/*
 * group.h
 *
 *  Created on: 2 juin 2016
 *      Author: user
 */

#ifndef GROUP_H_
#define GROUP_H_

#include <string>
#include <vector>
#include "Personne.h"
using namespace std;

namespace model
{
	class Group
	{
		private:
			string name;
			std::vector<Personne> tabPersonne;
			double totalExpenses;

		public:
			Group();
			Group(string inputName);
			Group(string inputName, std::vector<Personne> inputTabPersonne, double inputTotalExpenses);
			//Group(Group &Group);
			virtual ~Group();
			string getGroupName();
			void setGroupName(string inputGroupName);
			std::vector<Personne>& getTabPersonne();
			void addPersonneInGroup(Personne inputPersonne);
			double getTotalExpenses();
			void setTotalExpenses(double inputTotalExpenses);
			int getPersonneIndexInGroup(std::string name);
	};

	#endif /* GROUP_H_ */
}
