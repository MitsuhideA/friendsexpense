/*
 * DataExpenses.h
 *
 *  Created on: Jun 2, 2016
 *      Author: zak
 */

#ifndef MODEL_DATAEXPENSES_H_
#define MODEL_DATAEXPENSES_H_

#include <vector>

#include "Group.h"

namespace model {

struct DataExpenses {
	std::vector<Group> groupes;
};

} /* namespace model */

#endif /* MODEL_DATAEXPENSES_H_ */
