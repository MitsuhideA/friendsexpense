/*
 * Personne.cpp
 *
 *  Created on: 31 mai 2016
 *      Author: user
 */

#include "Personne.h"

namespace model
{

	Personne::Personne()
	{
		name = "Unknown";
		phoneNumber = "06XXXXXXXX";
		expenses = 0;
		deltaExpenses = 0;
	}

	Personne::Personne(string inputName, string inputPhoneNumber, double inputExpenses)
	{
		name = inputName;
		phoneNumber = inputPhoneNumber;
		expenses = inputExpenses;
		deltaExpenses = 0;
	}

	/*Personne::Personne(Personne &Personne)
	{
		name = Personne.name;
		phoneNumber = Personne.phoneNumber;
		expenses = Personne.expenses;
	}*/

	string Personne::getName()
	{
		return name;
	}

	void Personne::setName(string inputName)
	{
		name = inputName;
	}

	string Personne::getPhoneNumber()
	{
		return phoneNumber;
	}

	void Personne::setPhoneNumber(string inputPhoneNumber)
	{
		phoneNumber = inputPhoneNumber;
	}

	double Personne::getExpenses()
	{
		return expenses;
	}

	void Personne::setExpenses(double inputExpenses)
	{
		expenses = inputExpenses;
	}
	void Personne::addExpensesToPersonne(double inputExpenses)
	{
		expenses = expenses + inputExpenses;
	}

	double Personne::getDeltaExpenses()
	{
		return deltaExpenses;
	}

	void Personne::setDeltaExpenses(double inputDeltaExpenses)
	{
		deltaExpenses = inputDeltaExpenses;
	}

	Personne::~Personne()
	{

	}

}

