/*
 * CmdParser.cpp
 *
 *  Created on: Jun 2, 2016
 *      Author: zak
 */

#include "CmdParser.h"

namespace view
{
	CmdParser::CmdParser()
	{
		this->add<std::string>("file", 'f', "read the file to render the results", true, "");
		this->add<std::string>("name", 'n', "name of the person to add", false);
		this->add<std::string>("phone", 'p', "phone of the person to add", false);
		this->add<std::string>("expense", 'e', "expense of the person to add", false);
		this->add<std::string>("group", 'g', "group of the person to add", false);
		_data = NULL;
	}

	CmdParser::~CmdParser()
	{

	}

	void CmdParser::initData(DataExpenses* data)
	{
		_data = data;
	}

	void CmdParser::checkParser(int argc, char** argv)
	{
		this->parse_check(argc, argv);

		if(this->exist("file"))
		{
			if(this->exist("name") && this->exist("phone") && this->exist("expense") && this->exist("group"))
			{
				std::string sName = this->get<std::string>("name");
				std::string sPhone = this->get<std::string>("phone");
				std::string sGroup = this->get<std::string>("group");

				double dExpenses;
				try
				{
					dExpenses = std::stod(this->get<std::string>("expense"));
				} catch(...)
				{
					std::cout << "Bad argument input (expense)." << std::endl;
					exit(1);
				}

				CSVParser parser(this->get<std::string>("file"));
				parser.setNewLine(sName, sPhone, dExpenses, sGroup);

				std::cout << "Line added." << std::endl;
			}
			else if(this->exist("name") || this->exist("phone") || this->exist("expense") || this->exist("group"))
			{
				std::cout << "Error: options --name, --phone, --expense, and --group must be strictly used together." << std::endl;
				exit(1);
			}
			else
			{
				this->parseFile(this->get<std::string>("file"));
			}
		}
	}

	void CmdParser::parseFile(std::string file)
	{
		std::cout << "File:" << file << std::endl;

		ExpenseCalculator calculator;
		if(calculator.calculateFromFile(file, _data) != 0)
		{
			std::cout << "File is badly formatted." << std::endl;
			return;
		}
		for(unsigned int i=0; i < _data->groupes.size(); i++)
		{
			std::cout << BOLDRED;
			std::cout << "Group : " << _data->groupes.at(i).getGroupName() << " Total Expenses : " << _data->groupes.at(i).getTotalExpenses() << std::endl;
			std::cout << RESET;
			std::cout << "Name, PhoneNumber, Expenses : "<< std::endl;
			for(unsigned int j=0; j < _data->groupes.at(i).getTabPersonne().size(); j++)
			{
				std::cout 	<< _data->groupes.at(i).getTabPersonne().at(j).getName() << ", "
							<< _data->groupes.at(i).getTabPersonne().at(j).getPhoneNumber() << ", "
							<< _data->groupes.at(i).getTabPersonne().at(j).getExpenses() << ", ";
							if(_data->groupes.at(i).getTabPersonne().at(j).getDeltaExpenses() > 0)
							{
								std::cout<< " doit recevoir : " << _data->groupes.at(i).getTabPersonne().at(j).getDeltaExpenses();
							}
							else if (_data->groupes.at(i).getTabPersonne().at(j).getDeltaExpenses() < 0)
							{
								std::cout<< " doit rembourser : " << abs(_data->groupes.at(i).getTabPersonne().at(j).getDeltaExpenses());
							}
							else
							{
								std::cout<< " ne doit rien";
							}
							std::cout<< std::endl;
			}
			std::cout << std::endl;
		}
	}
}
